var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var prisons = require('./routes/prisons_routes');
var sectors = require('./routes/sectors_routes');
var prisoner = require('./routes/prisoner_routes');
var admins = require('./routes/admins_routes');
var guard = require('./routes/guard_routes');
var adminsRank = require('./routes/adminsRank_routes');
var guardRank = require('./routes/guardRank_routes');
var login = require('./routes/login_routes');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/prisons', prisons);
app.use('/sectors', sectors);
app.use('/prisoner', prisoner);
app.use('/admins', admins);
app.use('/guard', guard);
app.use('/adminsRank', adminsRank);
app.use('/guardRank', guardRank);
app.use('/login', login);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
