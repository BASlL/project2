var express = require('express');
var router = express.Router();
var login_dal = require('../model/login_dal');

router.get('/home', function(req, res) {
    login_dal.getCurrentUser(function (err, result2) {
        res.render('login/home', {user: result2});
    });
});

router.get('/register', function(req, res) {
            res.render('login/register');
    });


router.get('/login', function(req, res) {
    res.render('login/login', {was_successful: true});
});

router.get('/logout', function(req, res) {
    login_dal.deleteCurrent(function (err, result) {
        res.render('login/login', {was_successful: true});
    });
});

router.get('/verify', function (req, res) {
    login_dal.verify(req.query, function(err, result){

        if (result[0][0]) {
            login_dal.deleteCurrent(function (err, result2) {
                login_dal.insertCurrent(result[0][0], function (err, result3) {
                    login_dal.getCurrentUser(function (err, result4) {
                        res.render('login/home', {user: result4, was_successful: true});
                    });
                });
            });
        }
        else {
            res.render('login/login', {was_successful2: true});
        }
    });
});

router.get('/about', function(req, res) {
    login_dal.getCurrentUser(function (err, result2) {
        res.render('login/about', {user: result2, was_successful: true});
    });
});

// View the prisoner for the given id
router.get('/', function(req, res){
    if(req.query.prisoner_id == null) {
        res.send('prisoner_id is null');
    }
    else {
        prisoner_dal.getById(req.query.prisoner_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('prisoner/prisonerViewById', {prisoner: result[0][0], prisons: result[1], guard: result[2]});
            }
        });
    }
});

// Return the add a new prisoner form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    prisoner_dal.getAll2(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('prisoner/prisonerAdd', {'prisoner': result[0][0], 'sectors': result[1]});
        }
    });
});

// View the prisoner for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == null) {
        res.send('First Name must be provided.');
    }
    else if(req.query.last_name == null) {
        res.send('Last Name must be provided.');
    }
    else if(req.query.user_name == null) {
            res.send('User Name must be provided.');
    }
    else if(req.query.user_password == null) {
        res.send('Password must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        login_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                login_dal.verify(req.query, function(err, result){

                    if (result[0][0]) {
                        console.log("#result[0][0].first_name: " + result[0][0].first_name);

                        login_dal.deleteCurrent(function (err, result2) {
                            console.log("#result[0][0].first_name: " + result[0][0].first_name);

                            login_dal.insertCurrent(req.query, function (err, result2) {
                                console.log("#req.query.first_name: " + result[0][0].first_name);

                                login_dal.getCurrentUser(function (err, result3) {
                                    console.log("#CURRENT USER.first_name: " + result3.first_name);
                                    res.render('login/home', {user: result3, was_successful: true});
                                });
                            });
                        });
                    }
                    else {
                        res.render('login/login', {was_successful: false});
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.prisoner_id == null) {
        res.send('A prisoner id is required');
    }
    else {
        prisoner_dal.edit(req.query.prisoner_id, function(err, result){
            res.render('prisoner/prisonerUpdate', {prisoner: result[0][0], sectors: result[1]});
        });
    }

});

router.get('/update', function(req, res) {
    prisoner_dal.update(req.query, function(err, result){
        res.redirect(302, '/prisoner/all');
    });
});

// Delete a prisoner for the given prisoner_id
router.get('/delete', function(req, res){
    if(req.query.prisoner_id == null) {
        res.send('prisoner_id is null');
    }
    else {
        prisoner_dal.delete(req.query.prisoner_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/prisoner/all');
            }
        });
    }
});

module.exports = router;
