var express = require('express');
var router = express.Router();
var sectors_dal = require('../model/sectors_dal');
var prisons_dal = require('../model/prisons_dal');
var login_dal = require('../model/login_dal');


// View All sectors
router.get('/all', function(req, res) {
    sectors_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            login_dal.getCurrentUser(function (err, result2) {
                res.render('sectors/sectorsViewAll', {'result': result, 'user': result2});
            });
        }
    });

});

// View the sectors for the given id
router.get('/', function(req, res){
    if(req.query.sectors_id == null) {
        res.send('sectors_id is null');
    }
    else {
        sectors_dal.getById(req.query.sectors_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    res.render('sectors/sectorsViewById', {
                        sectors: result[0][0],
                        prisoner: result[1],
                        guard: result[2],
                        user: result2
                    });
                });
            }
        });
    }
});

// Return the add a new sectors form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    sectors_dal.getAll(function(err,result) {
        prisons_dal.getAll(function(err,result2) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result3) {
                    res.render('sectors/sectorsAdd', {'sectors': result, 'prisons': result2, 'user': result3});
                });
            }
        });
    });
});

// View the sectors for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.sectors_name == null) {
        res.send('Sector Name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        sectors_dal.insert(req.query, function(err,sectors_id) {
            if (err) {
                res.send(err);
            }
            else {
                login_dal.getCurrentUser(function (err, result2) {
                    sectors_dal.edit(sectors_id, function (err, result) {
                        res.render('sectors/sectorsUpdate', {
                            sectors: result[0][0],
                            prisons: result[1],
                            user: result2,
                            was_successful: true
                        });
                    });
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.sectors_id == null) {
        res.send('A sectors id is required');
    }
    else {
        login_dal.getCurrentUser(function (err, result2) {
            sectors_dal.edit(req.query.sectors_id, function(err, result) {
                res.render('sectors/sectorsUpdate', {sectors: result[0][0], prisons: result[1], user: result2});
            });
        });
    }

});

router.get('/update', function(req, res) {
    sectors_dal.update(req.query, function(err, result){
        res.redirect(302, '/sectors/all');
    });
});

// Delete a sectors for the given sectors_id
router.get('/delete', function(req, res){
    if(req.query.sectors_id == null) {
        res.send('sectors_id is null');
    }
    else {
        sectors_dal.delete(req.query.sectors_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/sectors/all');
            }
        });
    }
});

module.exports = router;
