var express = require('express');
var router = express.Router();
var login_dal = require('../model/login_dal');

/* GET home page. */
router.get('/', function(req, res, next) {
    login_dal.deleteCurrent(function (err, result) {
        res.render('index', {title: 'Public Prisons'});
    });
});

module.exports = router;
