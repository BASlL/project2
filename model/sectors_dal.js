var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT s.*, p.* FROM sectors s ' +
    'LEFT JOIN prisons p ON p.prisons_id = s.prisons_id ';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(sectors_id, callback) {
    var query = 'CALL sectors_getinfo(?)';
    var queryData = [sectors_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO sectors (sectors_name, prisons_id) VALUES (?,?)';

    var queryData = [params.sectors_name, params.prisons_id];

    connection.query(query, queryData, function(err, result) {
        var sectors_id = result.insertId;
        callback(err, sectors_id);
    });
};

exports.delete = function(sectors_id, callback) {
    var query = 'DELETE FROM sectors WHERE sectors_id = ?';
    var queryData = [sectors_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE sectors SET sectors_name = ?, prisons_id = ? WHERE sectors_id = ?';
    var queryData = [params.sectors_name, params.prisons_id, params.sectors_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
    DELIMITER //
    CREATE PROCEDURE sectors_getinfo (_sectors_id int)
    BEGIN

    SELECT * FROM sectors WHERE sectors_id = _sectors_id;

    SELECT s.*, p.*, p2.* FROM prisons p
    JOIN sectors s ON s.prisons_id = p.prisons_id
    LEFT JOIN prisoner p2 ON p2.sectors_id = s.sectors_id
    WHERE s.sectors_id = _sectors_id;

    SELECT s.*, p.*, g.* FROM prisons p
    JOIN sectors s ON s.prisons_id = p.prisons_id
    JOIN guard_sectors gs ON gs.sectors_id = s.sectors_id
    LEFT JOIN guard g ON  g.guard_id = gs.guard_id
    WHERE s.sectors_id = _sectors_id;


    END //
    DELIMITER ;

 */



exports.edit = function(sectors_id, callback) {
    var query = 'CALL sectors_getinfo2(?)';
    var queryData = [sectors_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};