var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view guard_view as
 select s.*, a.street, a.zip_code from guard s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT* FROM guard ORDER BY guard.last_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.verify = function(params, callback) {
    var query = 'CALL user_checkPassword(?,?)';
    var queryData = [params.user_name, params.user_password];

    connection.query(query, queryData, function(err, result) {
       callback(err, result);
    });
};

exports.deleteCurrent = function (callback) {
    var query = 'DELETE FROM currentUser';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insertCurrent = function (params, callback) {
    var query = 'INSERT INTO currentUser (first_name, last_name) VALUES (?,?)';
    var queryData = [params.first_name, params.last_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getAll2 = function(callback) {
    var query = 'CALL guard_getinfo_all()';

    connection.query(query, function(err, result) {

        callback(err, result);
    });
};

exports.getCurrentUser = function (callback) {
    var query = 'SELECT * FROM currentUser ';

    connection.query(query, function(err, result) {

        callback(err, result);
    });
};


exports.getById = function(guard_id, callback) {
    var query = 'CALL guard_getinfo(?)';
    var queryData = [guard_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE GUARD
    var query = 'INSERT INTO user (first_name, last_name, user_name, user_password) VALUES (?,?,?,?)';

    var queryData = [params.first_name, params.last_name, params.user_name, params.user_password];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(guard_id, callback) {
    var query = 'DELETE FROM guard_sectors WHERE guard_id = ?';
    var query2 = 'DELETE FROM guard WHERE guard_id = ?';
    var queryData = [guard_id];

    connection.query(query, queryData, function(err, result) {
        connection.query(query2, queryData, function(err, result) {
        callback(err, result);
        });
    });
};

//declare the function so it can be used locally
var guardAddressInsert = function(guard_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO guard_sectors (guard_id, sectors_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var guardAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            guardAddressData.push([guard_id, addressIdArray[i]]);
        }
    }
    else {
        guardAddressData.push([guard_id, addressIdArray]);
    }
    connection.query(query, [guardAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.guardAddressInsert = guardAddressInsert;

//declare the function so it can be used locally
var guardAddressDeleteAll = function(guard_id, callback){
    var query = 'DELETE FROM guard_sectors WHERE guard_id = ?';
    var queryData = [guard_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.guardAddressDeleteAll = guardAddressDeleteAll;


exports.update = function(params, callback) {
    var query = 'UPDATE guard SET first_name = ?, last_name = ?, rank_id=?, phone_no = ?, email = ? WHERE guard_id = ?';
    var queryData = [params.first_name, params.last_name, params.rank_id, params.phone_no, params.email, params.guard_id];

    connection.query(query, queryData, function(err, result) {
        //delete admins_address entries for this admins
        guardAddressDeleteAll(params.guard_id, function(err, result){

            if(params.sectors_id != null) {
                //insert admins_address ids
                guardAddressInsert(params.guard_id, params.sectors_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });
    });
};

/*  Stored procedure used in this example

    DELIMITER //
    CREATE PROCEDURE guard_getinfo_all ()
    BEGIN

      SELECT * FROM guard;

      SELECT s.*, p.* FROM sectors s
        LEFT JOIN prisons p ON p.prisons_id = s.prisons_id
        GROUP BY p.prisons_id;

      SELECT p.* FROM prisons p
      LEFT JOIN sectors s ON s.prisons_id = p.prisons_id;

      SELECT gr.* FROM guard_rank gr
      GROUP BY gr.rank_name;

    END //
    DELIMITER ;

    CREATE PROCEDURE guard_getinfo (_guard_id int)
    BEGIN

      SELECT * FROM guard WHERE guard_id =_guard_id;

      SELECT p.*, s.* FROM prisons p
      LEFT JOIN sectors s ON s.prisons_id = p.prisons_id
      LEFT JOIN guard_sectors gs ON gs.sectors_id = s.sectors_id
      LEFT JOIN guard g ON g.guard_id = gs.guard_id
      WHERE g.guard_id = _guard_id;

      SELECT gr.*, g.guard_id FROM guard_rank gr
      JOIN guard g ON g.rank_id = gr.rank_id AND guard_id = _guard_id;


    END //
    DELIMITER ;


    DELIMITER //
    CREATE PROCEDURE guard_getinfo3 (_guard_id int)
    BEGIN

      SELECT * FROM guard WHERE guard_id =_guard_id;

      SELECT s.*, p.prisons_id FROM guard_sectors gs
        LEFT JOIN guard g ON g.guard_id = gs.guard_id AND g.guard_id = _guard_id
        LEFT JOIN sectors s ON s.sectors_id = gs.sectors_id
        JOIN prisons p ON p.prisons_id = s.prisons_id
        WHERE s.prisons_id IN (SELECT p.prisons_id FROM prisons p
                LEFT JOIN sectors s ON s.prisons_id = p.prisons_id
                LEFT JOIN guard_sectors gs ON gs.sectors_id = s.sectors_id
                JOIN guard g ON g.guard_id = gs.guard_id AND g.guard_id = _guard_id)
          GROUP BY s.sectors_id;

      SELECT p.*, g.guard_id FROM prisons p
      LEFT JOIN sectors s ON s.prisons_id = p.prisons_id
      LEFT JOIN guard_sectors gs ON gs.sectors_id = s.sectors_id
      JOIN guard g ON g.guard_id = gs.guard_id AND g.guard_id = _guard_id;

      SELECT gr.*, g.guard_id FROM get_guard_rank_view gr
        LEFT JOIN guard g ON g.rank_id = gr.rank_id AND guard_id = _guard_id
        WHERE gr.prisons_id IN (SELECT p.prisons_id FROM prisons p
            LEFT JOIN sectors s ON s.prisons_id = p.prisons_id
            LEFT JOIN guard_sectors gs ON gs.sectors_id = s.sectors_id
            JOIN guard g ON g.guard_id = gs.guard_id AND g.guard_id = _guard_id);

    END //
    DELIMITER ;

 */

exports.edit = function(guard_id, callback) {
    var query = 'CALL guard_getinfo3(?)';
    var queryData = [guard_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};