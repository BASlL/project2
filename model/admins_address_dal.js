var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAllPrisons = function(callback) {
    var query = 'SELECT * FROM prisons;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getAllRank = function(callback) {
    var query = 'SELECT * FROM admins_rank;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};