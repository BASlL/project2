var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view guard_view as
 select s.*, a.street, a.zip_code from guard s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT* FROM guard_rank';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(rank_id, callback) {
    var query = 'CALL guardRank_getinfo(?)';
    var queryData = [rank_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE GUARD
    var query = 'INSERT INTO guard_rank (rank_name) VALUES (?)';

    var queryData = [params.rank_name];

    connection.query(query, queryData, function(err, result) {
        // THEN USE THE ACCOUNT_ID RETURNED AS insertId AND THE SELECTED SCHOOL_IDs INTO ACCOUNT_SCHOOL
        var rank_id = result.insertId;
        callback(err, rank_id);
    });
};

exports.delete = function(rank_id, callback) {
    var query = 'DELETE FROM guard_rank WHERE rank_id = ?';
    var queryData = [rank_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE guard_rank SET rank_name = ? WHERE rank_id = ?';
    var queryData = [params.rank_name, params.rank_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS guardRank_getinfo;

    DELIMITER //
    CREATE PROCEDURE guardRank_getinfo (_rank_id int)
    BEGIN

      SELECT * FROM guard_rank WHERE rank_id =_rank_id;

      SELECT a.* FROM guard a WHERE rank_id = _rank_id;

    END //
    DELIMITER ;

    CALL adminsRank_getinfo(1);


    DROP PROCEDURE IF EXISTS guardRank_getinfo2;

    DELIMITER //
    CREATE PROCEDURE guardRank_getinfo2 (_rank_id int)
    BEGIN

      SELECT * FROM guard_rank WHERE rank_id =_rank_id;

    END //
    DELIMITER ;

    SELECT * FROM admins_rank WHERE rank_id = 1;

    CALL guardRank_getinfo2(1);

 */

exports.edit = function(guard_id, callback) {
    var query = 'CALL guardRank_getinfo2(?)';
    var queryData = [guard_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};