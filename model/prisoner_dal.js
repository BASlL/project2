var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view prisoner_view as
 select s.*, a.street, a.zip_code from prisoner s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT* FROM prisoner ORDER BY prisoner.last_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getAll2 = function(callback) {
    var query = 'CALL prisoner_getinfo_all()';

    connection.query(query, function(err, result) {

        callback(err, result);
    });
};


exports.getById = function(prisoner_id, callback) {
    var query = 'CALL prisoner_getinfo(?)';
    var queryData = [prisoner_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO prisoner (first_name, last_name, sectors_id, admit_date, release_date) VALUES (?,?,?,?,?)';

    var queryData = [params.first_name, params.last_name, params.sectors_id, params.admit_date, params.release_date];

    connection.query(query, queryData, function (err, result) {
        var prisoner_id = result.insertId;
        callback(err, prisoner_id);
    });
};


exports.delete = function(prisoner_id, callback) {
    var query = 'DELETE FROM prisoner WHERE prisoner_id = ?';
    var queryData = [prisoner_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE prisoner SET first_name = ?, last_name = ?, sectors_id = ?, admit_date = ?, release_date = ? WHERE prisoner_id = ?';
    var queryData = [params.first_name, params.last_name, params.sectors_id, params.admit_date, params.release_date, params.prisoner_id];

    connection.query(query, queryData, function(err, result) {
            callback(err, result);
    });
};

/*  Stored procedure used in this example
    DELIMITER //
    CREATE PROCEDURE prisoner_getinfo_all ()
    BEGIN

      SELECT * FROM prisoner;

      SELECT s.*, p.* FROM sectors s
      LEFT JOIN prisons p ON p.prisons_id = s.prisons_id;

    END //
    DELIMITER ;

    CALL prisoner_getinfo_all();

    DELIMITER //
    CREATE PROCEDURE prisoner_getinfo (_prisoner_id int)
    BEGIN

    SELECT * FROM prisoner WHERE prisoner_id =_prisoner_id;

      SELECT pr.*, p.*, s.* FROM prisoner pr
      LEFT JOIN sectors s ON s.sectors_id = pr.sectors_id
      LEFT JOIN prisons p ON p.prisons_id = s.prisons_id
      WHERE pr.prisoner_id = _prisoner_id;

      SELECT g.*, gr.rank_name FROM guard g
        LEFT JOIN guard_sectors gs ON gs.guard_id = g.guard_id
        LEFT JOIN guard_rank gr ON gr.rank_id = g.rank_id
        LEFT JOIN sectors s ON s.sectors_id = gs.sectors_id
        LEFT JOIN prisoner p ON p.sectors_id = s.sectors_id
        WHERE p.prisoner_id = _prisoner_id;

    END //
    DELIMITER ;

    DELIMITER //
    CREATE PROCEDURE prisoner_getinfo2 (_prisoner_id int)
    BEGIN

      SELECT * FROM prisoner WHERE prisoner_id =_prisoner_id;

      SELECT pr.prisons_name, s.*, p.* FROM sectors s
      LEFT JOIN prisoner p ON p.sectors_id = s.sectors_id AND p.prisoner_id = _prisoner_id
      LEFT JOIN prisons pr ON pr.prisons_id = s.prisons_id;

    END //
    DELIMITER ;

 */

exports.edit = function(prisoner_id, callback) {
    var query = 'CALL prisoner_getinfo2(?)';
    var queryData = [prisoner_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};