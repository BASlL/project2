var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM prisons;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(prisons_id, callback) {
    var query = 'SELECT * FROM prisons WHERE prisons_id = ? ';

    var queryData = [prisons_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO prisons (prisons_name, street, zip_code) VALUES (?, ?, ?)';
    var queryData = [params.prisons_name, params.street, params.zip_code];

    connection.query(query, queryData, function (err, result) {
        var prisons_id = result.insertId;
        callback(err, prisons_id);
    });
};

exports.delete = function(prisons_id, callback) {
    var query = 'DELETE FROM prisons WHERE prisons_id = ?';
    var queryData = [prisons_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE prisons SET prisons_name = ?, street = ?, zip_code = ? WHERE prisons_id = ?';
    var queryData = [params.prisons_name, params.street, params.zip_code, params.prisons_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
     DELIMITER //
    CREATE PROCEDURE prisons_getinfo (_prisons_id int)
    BEGIN

    SELECT * FROM prisons WHERE prisons_id = _prisons_id;

    END //
    DELIMITER ;

    SELECT * FROM sectors;


 */

exports.edit = function(prisons_id, callback) {
    var query = 'CALL prisons_getinfo(?)';
    var queryData = [prisons_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};